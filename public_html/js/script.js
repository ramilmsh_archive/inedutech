var index = 100;
$("#menu > div").eq(index).css("display", "block");
$("#menu > p").click(function () {
  if ($("#menu").css("height") != "50px") {
    $("#menu").animate({
      height: "50px"
    }, 700);
    $("#menu > div").animate({
      opacity: 0
    }, 700);
    $("#menu > div").css("display", "none");
  } else {
    index = 100;
  }
  if (index != $(this).index()) {
    index = $(this).index();
    $("#menu").animate({
      height: "500px"
    }, 700);
    $("#menu > div").eq(index).animate({
      opacity: 1
    }, 700);
    $("#menu > div").eq(index).css("display", "block");
  }
});

function resize(obj) {
  $("body").append("<div id='bg'></div>");
  var el = new Image();
  $(el).attr("src", $(obj).attr("src"));
  $(el).attr("class", "temp");
  $("#bg").append(el);
  var w, h;
  if ($(el).width() / $(el).height() > screen.width / screen.height) {
    w = $("#bg").width() * 0.9;
    $(el).width(w);
    h = $(el).height();
  } else {
    h = $("#bg").height() * 0.9;
    $(el).height(h);
    w = $(el).width();
  }
  $(el).css({
    top: obj.offsetTop - window.scrollY,
    left: obj.offsetLeft - window.scrollX,
    width: $(obj).width(),
    height: $(obj).height()
  });
  $("#bg").animate({
    opacity: 1
  }, 1000)
  $(el).animate({
    top: "50%",
    marginTop: -h / 2,
    marginLeft: -w / 2,
    left: "50%",
    width: w,
    height: h
  }, 1000);
  $(el).click(function () {
    $(el).animate({
      top: obj.offsetTop - window.scrollY,
      left: obj.offsetLeft - window.scrollX,
      width: $(obj).width(),
      height: $(obj).height(),
      marginTop: 0,
      marginLeft: 0
    }, 1000).queue(function () {
      $("#bg").animate({
        opacity: 0,
      }, 100).queue(function () {
        $(el).remove();
        $("#bg").remove();
      });
    })
  })
}

function load(x) {
  $("#text").append("<div id='loading'><img style='alingn:center' src='img/loading.gif' /> Загрузка</div>");
  $.get("data/" + x, function (data) {
    $("#text").html(data);
  });
  $("#menu").animate({
    height: "50px"
  }, 700);
  $("#menu > div").animate({
    opacity: 0
  }, 700);
  $("#menu > div").css("display", "none");
}
load("0.php");
$(".to").click(function () {
  load(this.rel);
});

function send() {
  $.ajax({
    type: "POST",
    url: "data/feedback.php",
    data: {
      name: $("[name='name']").val(),
      email: $("[name='email']").val(),
      title: $("[name='title']").val(),
      text: $("[name='text']").val()
    },
    success: function (data) {
      popup(data);
      if (data == "Отзыв отправлен") {
        $("[name='name']").val("");
        $("[name='email']").val("");
        $("[name='title']").val("");
        $("[name='text']").val("");
      }
    }
  });
}
function img_hover(x, e) {
  e.preventDefault();
  $("#text").append("<div id='hover_div'>Нажмите, чтобы рассмотреть</div>");
  window.onmousemove = function (e) {
    $("#hover_div").css({
      position: "absolute",
      top: e.pageY + 20,
      left: e.pageX + 20,
      cursor: "-webkit-zoom-in",
      cursor: "-moz-zoom-in",
      background: "white",
      opacity: .7,
      padding: 5
    });
  }
  x.onmouseout = function () {
  	$("#hover_div").remove();
  }
}
function popup (x) {
	$("#main").append("<div id='popup'>" + x + "</div>");
    $("#popup").css({
      marginTop: -$("#popup").height() / 2,
      marginLeft: -$("#popup").width() / 2
    });
  	$("#popup").click(function () {
      $(this).remove();
    });
    $("#popup").delay(4000).animate({
        opacity: 0
    }, 500).queue(function () {
      $(this).remove();
    });
}